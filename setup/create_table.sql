DROP TABLE IF EXISTS `weibo_attitude`;
CREATE TABLE `weibo_attitude` (
  `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `result_time` VARCHAR(255) NOT NULL,
  `wbnum_pos` INT UNSIGNED NOT NULL,
  `wbnum_neg` INT UNSIGNED NOT NULL,
  `wbnum_neu` INT UNSIGNED NOT NULL,
  KEY (`wbnum_pos`)
  KEY (`wbnum_neg`)
  KEY (`result_time`)
) CHARACTER SET utf8mb4;

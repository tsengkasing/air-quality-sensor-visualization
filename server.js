/**
 * @fileoverview
 * server entry
 */

const path = require('path');
const { logInfo, logError } = require('./service/logger');

/**
 * Load configuration
 * @type {{database: {host: string, port: number, username: string, password: string, schema: string}, server_port: number}}
 */
let config;
try {
    config = require('./config');
} catch (e) {
    console.error(e.message + ' Configuration File Not Found! Please copy config.template.js to config.js & write your configuration.');
    process.exit(1);
}

const {database: {host, port, username, password, schema}, server_port: SERVER_PORT} = config;

const DB = require('./service/db');
const fastify = require('fastify')({
    // logger: true
});

// Content type parser for 'application/x-www-form-urlencoded'
fastify.register(require('fastify-formbody'));

// Cookie Plugin
fastify.register(require('fastify-cookie'));

// Connect mysql
fastify.register(require('fastify-mysql'), {
    promise: true,
    connectionString: `mysql://${username}:${password}@${host}:${port}/${schema}`
});

// Serve static files
fastify.register(require('fastify-static'), {
    root: path.join(__dirname, 'public'),
    prefix: '/'
});

// Load Controllers
fastify.register(require('./controller/dashboard'), { prefix: '/api/dashboard' });
fastify.register(require('./controller/search'), { prefix: '/api/search' });

// Response Content Type
fastify.addHook('onSend', (request, reply, payload, next) => {
    if (/\/api/.test(request.raw.url)) {
        reply.type('application/json; charset=utf8');
    }
    next();
});

// Start Server
fastify.listen(SERVER_PORT, '0.0.0.0', function (err, address) {
    if (err) {
        fastify.log.error(err);
        process.exit(1);
    }
    fastify.mysql.getConnection().then(conn => {
        const db = new DB(conn);
        fastify.decorate('db', db);
        logInfo(`server listening on ${address}`);
    }).catch(err => {
        logError(err);
        process.exit(1);
    });
});

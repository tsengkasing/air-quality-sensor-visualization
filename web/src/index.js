import React, { useState, lazy, Suspense } from 'react';
import ReactDOM from 'react-dom';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import pink from '@material-ui/core/colors/pink';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import Dashboard from './components/dashboard';
const SearchByTimeRange = lazy(
    () => import(/* webpackChunkName: "SearchByTimeRange", webpackMode: "lazy" */ './components/search-by-time-range')
);
const SearchByNum = lazy(
    () => import(/* webpackChunkName: "SearchByNum", webpackMode: "lazy" */ './components/search-by-num')
);

// App Theme
const theme = createMuiTheme({
    palette: {
        primary: blue,
        secondary: pink,
    },
    typography: {
        useNextVariants: true,
    },
});

function App() {
    const [tabIndex, setTabIndex] = useState(0);
    return (
        <MuiThemeProvider theme={theme}>
            <Paper style={{flexGrow: 1, margin: 0, width: '100%'}}>
                <Tabs
                    value={tabIndex}
                    onChange={(_, value) => setTabIndex(value)}
                    indicatorColor="primary"
                    textColor="primary"
                    centered
                >
                    <Tab label="Dashboard" />
                    <Tab label="Search By Time Range" />
                    <Tab label="Search By Num" />
                </Tabs>
            </Paper>
            <div className="tab-container"
                style={{display: `${tabIndex === 0 ? 'block' : 'none'}`}}>
                <Dashboard />
            </div>
            <div className="tab-container"
                style={{display: `${tabIndex === 1 ? 'block' : 'none'}`}}>
                <Suspense fallback={<p>Loading...</p>}>
                    <SearchByTimeRange />
                </Suspense>
            </div>
            <div className="tab-container"
                style={{display: `${tabIndex === 2 ? 'block' : 'none'}`}}>
                <Suspense fallback={<p>Loading...</p>}>
                    <SearchByNum />
                </Suspense>
            </div>
        </MuiThemeProvider>
    );
}

ReactDOM.render(<App />, document.getElementById('root'));

import React, { useState, useEffect } from 'react';
import {
    ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

import '../stylus/dashboard.styl';

import Checkbox from '@material-ui/core/Checkbox';
import { formatDate } from '../util/tool';
import { fetchRecentStatistic, fetchStreamingStatistic } from '../util/api';

function Dashboard() {
    const [timelineData, setTimelineData] = useState([]);
    const [showPositive, switchPositive] = useState(true);
    const [showNegative, switchNegative] = useState(true);
    const [showDiff, switchDiff] = useState(false);
    const [showAttention, switchAttention] = useState(false);
    const [showNeutral, switchNeutral] = useState(false);

    useEffect(() => {
        let lastId = null;

        fetchRecentStatistic(95).then(data => {
            // update last id
            lastId = data[data.length - 1].id;

            setTimelineData(data.map(({ result_time, wbnum_pos, wbnum_neg, wbnum_neu, ...props }) => ({
                result_time: formatDate(new Date(parseInt(result_time, 10))),
                wbnum_pos: parseFloat((wbnum_pos / (wbnum_pos + wbnum_neg + wbnum_neu) * 100).toFixed(2)),
                wbnum_neg: parseFloat((wbnum_neg / (wbnum_pos + wbnum_neg + wbnum_neu) * 100).toFixed(2)),
                attention: wbnum_pos + wbnum_neg,
                diff: wbnum_pos > wbnum_neg ? 4 : (wbnum_pos < wbnum_neg ? 2 : 3),
                ...props
            })));
        }).catch(err => {
            alert(err);
        });

        // schedule to run every 15 seconds
        let timer = setInterval(() => {
            fetchStreamingStatistic(lastId).then(data => {
                if (data.length <= 0) return;
                // update last id
                lastId = data[data.length - 1].id;
                setTimelineData(timelineData =>
                    [
                        ...timelineData.slice(1),
                        ...data.map(({ result_time, wbnum_pos, wbnum_neg, ...props }) => ({
                            result_time: formatDate(new Date(parseInt(result_time, 10))),
                            wbnum_pos: parseFloat((wbnum_pos / (wbnum_pos + wbnum_neg + wbnum_neu) * 100).toFixed(2)),
                            wbnum_neg: parseFloat((wbnum_neg / (wbnum_pos + wbnum_neg + wbnum_neu) * 100).toFixed(2)),
                            attention: wbnum_pos + wbnum_neg,
                            diff: wbnum_pos > wbnum_neg ? 4 : (wbnum_pos < wbnum_neg ? 2 : 3),
                            ...props
                        }))
                    ]
                );
            }).catch(err => {
                console.error(err);
            });
        }, 15 * 1000);
        return () => clearInterval(timer);
    }, []);

    return (
        <section className="dashboard__layout">
            <ResponsiveContainer width="90%" aspect={2}>
                <LineChart
                    data={timelineData}
                    margin={{
                        top: 5, right: 30, left: 20, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="result_time" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    {showPositive && <Line type="monotone" dataKey="wbnum_pos" stroke="#0f9d58" name="Positive" activeDot={{ r: 8 }} />}
                    {showNegative && <Line type="monotone" dataKey="wbnum_neg" stroke="#db4437" name="Negative" />}
                    {showDiff && <Line type="monotone" dataKey="diff" stroke="#000000" name="Diff" />}
                    {showAttention && <Line type="monotone" dataKey="attention" stroke="#0AF" name="Attention" />}
                    {showNeutral && <Line type="monotone" dataKey="wbnum_neu" stroke="#4285f4" name="Neutral" />}
                </LineChart>
            </ResponsiveContainer>
            <div className="controls">
                <div className="control">
                    <label>Positive num</label>
                    <Checkbox
                        checked={showPositive}
                        onChange={e => switchPositive(e.target.checked)}
                        value="Positive num"
                        style={showPositive ? {
                            color: '#0f9d58'
                        } : {}}
                    />
                </div>
                <div className="control">
                    <label>Diff </label>
                    <Checkbox
                        checked={showDiff}
                        onChange={e => switchDiff(e.target.checked)}
                        value="Diff"
                        style={showDiff ? {
                            color: '#000000'
                        } : {}}
                    />
                </div>
                <div className="control">
                    <label>Attention </label>
                    <Checkbox
                        checked={showAttention}
                        onChange={e => switchAttention(e.target.checked)}
                        value="Diff"
                        style={showAttention ? {
                            color: '#0AF'
                        } : {}}
                    />
                </div>
                <div className="control">
                    <label>Negative num</label>
                    <Checkbox
                        checked={showNegative}
                        onChange={e => switchNegative(e.target.checked)}
                        value="Negative num"
                        color="secondary"
                    />
                </div>
                <div className="control">
                    <label>Neutral num</label>
                    <Checkbox
                        checked={showNeutral}
                        onChange={e => switchNeutral(e.target.checked)}
                        value="Neutral num"
                        color="primary"
                    />
                </div>
            </div>
        </section>
    );
}

export default Dashboard;

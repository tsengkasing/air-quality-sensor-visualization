import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import { fetchStatisticOfTimePeriod } from '../util/api';
import { formatDateTime } from '../util/tool';

import '../stylus/search-by-num.styl';

function SearchByNum() {
    const [positiveCount, setPositiveCount] = useState('0');
    const [negativeCount, setNegativeCount] = useState('0');
    const [neutralCount, setNeutralCount] = useState('0');
    const [timePeriods, setTimePeriods] = useState([]);
    const handleInput = {
        'positive': e => setPositiveCount(e.target.value),
        'negative': e => setNegativeCount(e.target.value),
        'neutral': e => setNeutralCount(e.target.value),
    };

    function handleSubmitSearch() {
        fetchStatisticOfTimePeriod(
            positiveCount, negativeCount, neutralCount
        ).then(data => {
            setTimePeriods(data);
        }).catch(err => {
            alert(err);
        });
    }

    return (
        <section className="search-by-num__layout">
            <div className="input__layout">
                <div className="input__field">
                    <TextField
                        label="Positive >="
                        className=""
                        value={positiveCount}
                        onChange={handleInput['positive']}
                        margin="normal"
                    />
                </div>
                <div className="input__field">
                    <TextField
                        label="Negative >="
                        className=""
                        value={negativeCount}
                        onChange={handleInput['negative']}
                        margin="normal"
                    />
                </div>
                <div className="input__field">
                    <TextField
                        label="Neutral >="
                        className=""
                        value={neutralCount}
                        onChange={handleInput['neutral']}
                        margin="normal"
                    />
                </div>
                <Button variant="contained" color="primary" onClick={handleSubmitSearch}>Search</Button>
            </div>
            <div className="container">
                {timePeriods.map(({id, result_time, wbnum_neg, wbnum_neu, wbnum_pos}) => (
                    <Chip
                        key={id}
                        label={formatDateTime(new Date(parseInt(result_time, 10)))}
                        clickable
                        color="primary"
                    />
                ))}
            </div>
        </section>
    );
}

export default SearchByNum;

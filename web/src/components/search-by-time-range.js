import React, { useState } from 'react';
import {
    ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import { MuiPickersUtilsProvider , DateTimePicker } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';
import { formatTime } from '../util/tool';
import { fetchStatisticByTimeRange } from '../util/api';

import '../stylus/search-by-time-range.styl';


function SearchByTimeRange() {
    const [timelineData, setTimelineData] = useState([]);
    const [fromDate, handleFromDateChange] = useState(new Date());
    const [toDate, handleToDateChange] = useState(new Date());
    const [showPositive, switchPositive] = useState(true);
    const [showNegative, switchNegative] = useState(true);
    const [showNeutral, switchNeutral] = useState(false);

    function handleSubmitSearch() {
        if (toDate <= fromDate) return alert('"From" DateTime must before "To" DateTime');
        fetchStatisticByTimeRange(
            fromDate.getTime(),
            toDate.getTime()
        ).then(data => {
            setTimelineData(data.map(({ result_time, ...props }) => ({
                result_time: formatTime(new Date(parseInt(result_time, 10))),
                ...props
            })));
        }).catch(err => {
            alert(err);
        });
    }

    return (
        <section className="search-by-time-range__layout">
            <div className="input__layout">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <div className="time-picker">
                        <DateTimePicker
                            autoOk
                            ampm={false}
                            disableFuture
                            value={fromDate}
                            onChange={handleFromDateChange}
                            label="From"
                        />
                    </div>
                    <span> ~ </span>
                    <div className="time-picker">
                        <DateTimePicker
                            autoOk
                            ampm={false}
                            disableFuture
                            value={toDate}
                            onChange={handleToDateChange}
                            label="To"
                        />
                    </div>
                    <Button variant="contained" color="primary" onClick={handleSubmitSearch}>Search</Button>
                </MuiPickersUtilsProvider>
            </div>
            {timelineData.length > 0 && <ResponsiveContainer width="90%" aspect={2}>
                <LineChart
                    data={timelineData}
                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="result_time" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    {showPositive && <Line type="monotone" dataKey="wbnum_pos" stroke="#0f9d58" name="Positive" activeDot={{ r: 8 }} />}
                    {showNegative && <Line type="monotone" dataKey="wbnum_neg" stroke="#db4437" name="Negative" />}
                    {showNeutral && <Line type="monotone" dataKey="wbnum_neu" stroke="#4285f4" name="Neutral" />}
                </LineChart>
            </ResponsiveContainer>}
            {timelineData.length > 0 && <div className="controls">
                <div className="control">
                    <label>Positive num</label>
                    <Checkbox
                        checked={showPositive}
                        onChange={e => switchPositive(e.target.checked)}
                        value="Positive num"
                        style={showPositive ? {
                            color: '#0f9d58'
                        } : {}}
                    />
                </div>
                <div className="control">
                    <label>Negative num</label>
                    <Checkbox
                        checked={showNegative}
                        onChange={e => switchNegative(e.target.checked)}
                        value="Negative num"
                        color="secondary"
                    />
                </div>
                <div className="control">
                    <label>Neutral num</label>
                    <Checkbox
                        checked={showNeutral}
                        onChange={e => switchNeutral(e.target.checked)}
                        value="Neutral num"
                        color="primary"
                    />
                </div>
            </div>}
        </section>
    );
}

export default SearchByTimeRange;

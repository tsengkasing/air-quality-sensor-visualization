/**
 * @fileoverview
 * API list
 */

/**
 * Recent statistic API
 * @param {number} count
 * @returns {Promise<Array<{id: number, result_time: string, wbnum_neg: number, wbnum_neu: number, wbnum_pos: number}>>}
 */
export function fetchRecentStatistic(count = 30) {
    return new Promise((resolve, reject) => {
        fetch(`/api/dashboard/recent?count=${count}`, {
            method: 'GET'
        }).then(
            response => response.json()
        ).then(({status, msg, data}) => {
            if (status === 0) {
                resolve(data);
            } else {
                reject(msg);
            }
        }).catch(err => reject(err));
    });
}

/**
 * Streaming data
 * @param {string} lastId
 * @returns {Promise<Array<{id: number, result_time: string, wbnum_neg: number, wbnum_neu: number, wbnum_pos: number}>>}
 */
export function fetchStreamingStatistic(lastId) {
    return new Promise((resolve, reject) => {
        fetch(`/api/dashboard/streaming?lastId=${lastId}`, {
            method: 'GET'
        }).then(
            response => response.json()
        ).then(({status, msg, data}) => {
            if (status === 0) {
                resolve(data);
            } else {
                reject(msg);
            }
        }).catch(err => reject(err));
    });
}

/**
 * Search data by time range
 * @param {number} fromDate
 * @param {number} toDate
 * @returns {Promise<Array<{id: number, result_time: string, wbnum_neg: number, wbnum_neu: number, wbnum_pos: number}>>}
 */
export function fetchStatisticByTimeRange(fromDate, toDate) {
    return new Promise((resolve, reject) => {
        fetch(`/api/search/timerange?from=${fromDate}&to=${toDate}`, {
            method: 'GET'
        }).then(
            response => response.json()
        ).then(({status, msg, data}) => {
            if (status === 0) {
                resolve(data);
            } else {
                reject(msg);
            }
        }).catch(err => reject(err));
    });
}

/**
 * Search data by num
 * @param {number} positive
 * @param {number} negative
 * @param {number} neutral
 * @returns {Promise<Array<{id: number, result_time: string, wbnum_neg: number, wbnum_neu: number, wbnum_pos: number}>>}
 */
export function fetchStatisticOfTimePeriod(positive, negative, neutral) {
    return new Promise((resolve, reject) => {
        fetch(`/api/search/timeperiod?positive=${positive}&negative=${negative}&neutral=${neutral}`, {
            method: 'GET'
        }).then(
            response => response.json()
        ).then(({status, msg, data}) => {
            if (status === 0) {
                resolve(data);
            } else {
                reject(msg);
            }
        }).catch(err => reject(err));
    });
}

// export function fetchSomething() {
//     return new Promise((resolve, reject) => {
//         fetch(`/api/`, {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/x-www-form-urlencoded',
//                 'cache-control': 'no-store',
//             },
//             body: ``
//         }).then(
//             response => response.json()
//         ).then(({status, msg}) => {
//             if (status === 0) {
//                 resolve();
//             } else {
//                 reject(msg);
//             }
//         }).catch(err => reject(err));
//     });
// }

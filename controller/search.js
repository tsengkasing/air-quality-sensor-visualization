/**
 * @fileoverview
 * account controller
 */

const { logError } = require('../service/logger');
const { responseJSON } = require('../service/response');

module.exports = function (fastify, options, next) {
    /**
     * Fetch Timeline of a certain time range
     */
    fastify.get('/timerange', async (req, reply) => {
        let { from, to } = req.query;
        from = parseInt(from, 10);
        to = parseInt(to, 10);
        // in case the date is null
        if (isNaN(from) || isNaN(to)) {
            from = new Date();
            from.setSeconds(from.getSeconds() + 30);
            from = from.getTime();
            to = (new Date()).getTime();
        }
        try {
            /**
             * @description fetch latest 30 rows
             * @type {Array<{id: number, result_time: string, wbnum_neg: number, wbnum_neu: number, wbnum_pos: number}>}
             */
            let result = await fastify.db.fetchAll(`
                SELECT
                    id,
                    result_time,
                    wbnum_pos,
                    wbnum_neg,
                    wbnum_neu
                FROM
                    weibo_attitude
                WHERE
                    result_time > ?
                        AND result_time < ?
                LIMIT 300;
            `, [from, to]);
            if (!result) {
                return responseJSON(1, 'No Data');
            }

            return responseJSON(0, 'OK', result);
        } catch (err) {
            logError(err.message);
            return responseJSON(1, err.message);
        }
    });
    /**
     * Fetch time period
     */
    fastify.get('/timeperiod', async (req, reply) => {
        let { positive, negative, neutral } = req.query;
        [positive, negative, neutral] = [
            positive, negative, neutral
        ].map(num => parseInt(num, 10)).map(num => isNaN(num) ? 0 : num);
        try {
            /**
             * @description fetch latest 30 rows
             * @type {Array<{id: number, result_time: string, wbnum_neg: number, wbnum_neu: number, wbnum_pos: number}>}
             */
            let result = await fastify.db.fetchAll(`
                SELECT
                    id,
                    result_time,
                    wbnum_pos,
                    wbnum_neg,
                    wbnum_neu
                FROM
                    weibo_attitude
                WHERE
                    wbnum_pos >= ?
                        AND wbnum_neg >= ?
                        AND wbnum_neu >= ?
                LIMIT 300;
            `, [positive, negative, neutral]);
            if (!result) {
                return responseJSON(1, 'No Data');
            }

            return responseJSON(0, 'OK', result);
        } catch (err) {
            logError(err.message);
            return responseJSON(1, err.message);
        }
    });

    next();
};

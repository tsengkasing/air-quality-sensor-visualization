/**
 * @fileoverview
 * account controller
 */

const { logError } = require('../service/logger');
const { responseJSON } = require('../service/response');

module.exports = function (fastify, options, next) {
    /**
     * Fetch Recent Timeline
     */
    fastify.get('/recent', async (req, reply) => {
        let { count } = req.query;
        count = parseInt(count, 10);
        // in case the count is too large or null
        if (isNaN(count) || count > 1000) {
            count = 30;
        }
        try {
            /**
             * @description fetch latest 30 rows
             * @type {Array<{id: number, result_time: string, wbnum_neg: number, wbnum_neu: number, wbnum_pos: number}>}
             */
            let result = await fastify.db.fetchAll(`
                SELECT
                    id,
                    result_time,
                    wbnum_pos,
                    wbnum_neg,
                    wbnum_neu
                FROM
                    weibo_attitude
                ORDER BY
                    id DESC
                LIMIT ?;
            `, [count]);
            if (!result) {
                return responseJSON(1, 'No Data');
            }

            return responseJSON(0, 'OK', result.reverse());
        } catch (err) {
            logError(err.message);
            return responseJSON(1, err.message);
        }
    });

    /**
     * Fetch Streaming Timeline
     */
    fastify.get('/streaming', async (req, reply) => {
        let { lastId } = req.query;
        // in case the lastId is null
        if (!lastId) {
            reply.code = 415;
            return responseJSON(1, 'Missing Parameter');
        }
        try {
            // fetch latest rows since the target id
            let result = await fastify.db.fetchAll(`
                SELECT
                    id,
                    result_time,
                    wbnum_pos,
                    wbnum_neg,
                    wbnum_neu
                FROM
                    weibo_attitude
                WHERE
                    id > ?;
            `, [lastId]);
            if (!result) {
                return responseJSON(1, 'No Data');
            }

            return responseJSON(0, 'OK', result);
        } catch (err) {
            logError(err.message);
            return responseJSON(1, err.message);
        }
    });

    next();
};

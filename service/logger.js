/**
 * Normal log
 * @param {Array<string>} msg
 */
exports.logInfo = function(...msg) {
    console.log(`[${new Date().toLocaleString({}, {
        timeZone: 'Asia/Shanghai'
    })}][INFO] ${msg.map(v => JSON.stringify(v, null, 2)).join('\t')}`);
};

/**
 * Erro log
 * @param {Array<string>} msg
 */
exports.logError = function(...msg) {
    console.error(`[${new Date().toLocaleString({}, {
        timeZone: 'Asia/Shanghai'
    })}][ERROR] ${msg.map(v => JSON.stringify(v, null, 2)).join('\t')}`);
};

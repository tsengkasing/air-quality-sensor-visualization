/**
 * @fileoverview
 * DB
 */

const { logError } = require('./logger');

class DB {
    constructor(connection) {
        this._connection = connection;
        this._timer = null;

        // Keep connection with database
        const ping = connection => {
            connection.ping(err => {
                if (err) logError(err);
            });
            this._timer = setTimeout(ping, 60 * 60 * 1000, connection);
        };
        ping(this._connection);
    }

    /**
     * Fetch All Record
     * @param {string} sql SQL query
     * @param {Array<any>} params paramsters
     * @returns {Promise<Array<any>>}
     */
    async fetchAll(sql, params) {
        const [result] = await this._connection.query(sql, params);
        if (!result) {
            return null;
        }
        return result;
    }

    /**
     * Fetch One Record
     * @param {string} sql SQL query
     * @param {Array<any>} params paramsters
     * @returns {Promise<any>}
     */
    async fetchOne(sql, params) {
        const [result] = await this._connection.query(sql, params);
        if (!result) {
            return null;
        }
        return result[0];
    }

    /**
     * Execute SQL
     * @param {string} sql SQL query
     * @param {Array<any>} params paramsters
     * @returns {Promise<any>}
     */
    async executeSql(sql, params) {
        const [result] = await this._connection.query(sql, params);
        if (!result) {
            return null;
        }
        return result;
    }
}

module.exports = DB;

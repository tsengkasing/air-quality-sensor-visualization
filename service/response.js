/**
 * @param {number} status
 * @param {string} msg
 * @param {any} data
 * @returns {{status: number, msg: string, data: any}}
 */
exports.responseJSON = function (status = 0, msg = 'OK', data = null) {
    return {status, msg, data};
};

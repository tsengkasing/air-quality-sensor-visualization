# AirSensorQuality Visualization

[![](https://img.shields.io/badge/fastify-2.0-brightgreen.svg?style=flat-square)](https://www.fastify.io/)

## :rocket: Get Started

### :package: Install dependencies

```shell
$ git clone https://gitlab.com/tsengkasing/air-quality-sensor-visualization
$ cd weather-app
$ npm install
$ cd web
$ npm install
```

### :hammer: Configuration

Create file `config.js` and setup database.

```shell
$ cp config.template.js config.js
$ vim config.js
```

Run the SQL files in directory `setup`

### :beer: Front End

```shell
$ cd weather-app/web
$ npm run dev # developing
$ npm run build # production
```

### :coffee: Server Side

```shell
$ cd weather-app
$ node server.js
```

Visit http://localhost:1234 。

## Project Structure

```shell
.
├── config.js
├── config.template.js
├── controller            # controller
│   └── dashboard.js
├── LICENSE
├── package.json
├── public                # distribution
│   └── favicon.ico
├── README.md
├── server.js             # server entry
├── service               # service
│   ├── db.js
│   ├── logger.js
│   └── response.js
├── setup
│   └── create_table.sql
└── web                   # front end
    ├── package.json
    ├── src
    │   ├── components
    │   │   ├── dashboard.js
    │   ├── index.html
    │   ├── index.js      # js entry
    │   ├── stylus
    │   │   ├── index.styl
    │   │   └── dashboard.styl
    │   └── util
    │       └── api.js
    └── webpack.config.js
```
